var gulp = require('gulp'),
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    connect = require('gulp-connect');

gulp.task('default', ['connect', 'html']);

gulp.task('connect', function(){
  connect.server({
    root: 'www',
    livereload: true
  });
});

gulp.task('html', function(){
  var html_path = 'www/**/*.html';
  var js_path = 'www/js/**/*.js';
  var css_path = 'www/css/**/*.css';

  gulp
    .src(html_path)
    .pipe(watch([html_path, js_path, css_path]))
    .pipe(connect.reload());
});
