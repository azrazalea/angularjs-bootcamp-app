(function (){
  'use strict';

  angular.module('bootcampApp', ['ngRoute'])
    .config(function($routeProvider){
      $routeProvider
        .when('/randomNumber', {
          templateUrl: 'partials/random.html',
          controller: 'randomCtrl',
          resolve: {
            randomNumbers: function(Stupid){
              return Stupid.getRandomNumbers();
            }
          }
        })
        .when('/', {
          templateUrl: 'partials/main.html',
          controller: 'noteCtrl',
          resolve: {
            defaultNote: function(Notes){
              return Notes.getDefaultNote();
            }
          }
        })
        .otherwise({
          redirectTo: '/'
        });
    });
})();
