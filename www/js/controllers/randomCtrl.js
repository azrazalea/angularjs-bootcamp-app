(function (){
  'use strict';

  angular.module('bootcampApp')
    .controller('randomCtrl', function(Stupid, $scope, $q, randomNumbers){
      function startGeneration(numbers){
        return Stupid.startNumberGeneration(numbers);
      }

      function logMessage(message){
        console.log(message);
      }

      var promiseOne = startGeneration(randomNumbers.slice(0, 3)),
          promiseTwo = startGeneration(randomNumbers.slice(3, 6)),
          promiseThree = startGeneration(randomNumbers.slice(6, 9));

      promiseOne.then(logMessage);

      promiseTwo.then(logMessage);

      promiseThree.then(logMessage);


      $q.all([promiseOne, promiseTwo, promiseThree]).then(function(data){
        console.log("All numbers found");
        console.log(data);
        $scope.message = data;
      });
    });
})();
