(function(){
  'use strict';

  angular.module('bootcampApp')
    .controller('noteCtrl', function(Notes, $scope, defaultNote){
      $scope.notes = Notes.getData();
      var fallbackNote = {
        "title": "Fallback title",
        "body": "Fallback body"
      };

      $scope.newNote = defaultNote.data;

      $scope.createNote = function(newNote){
        newNote.createdDate = new Date();
        $scope.notes.push(Notes.cloneNote(newNote));
        $scope.newNote = defaultNote.data;
        Notes.saveData($scope.notes);
      };

      $scope.deleteNote = function(note){
        $scope.notes.splice($scope.notes.indexOf(note), 1);
        Notes.saveData($scope.notes);
      };

      $scope.deleteAllNotes = function(){
        $scope.notes = [];
        Notes.saveData($scope.notes);
      };
    });
})();
