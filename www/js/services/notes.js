(function (){
  'use strict';

  angular.module('bootcampApp')
    .service('Notes', function($http){
      function cloneNote(note){
        var my_note = {};
        my_note.body = note.body;
        my_note.title = note.title;
        my_note.createdDate = note.createdDate;
        return my_note;
      }

      function saveData(data){
        window.localStorage['noteStorage'] = angular.toJson(data);
      }

      function getData(){
        var data = window.localStorage['noteStorage'] || [];
        return angular.fromJson(data);
      }

      function getDefaultNote(){
        return $http.get('default_note.json');
      }


      this.cloneNote = cloneNote;
      this.saveData = saveData;
      this.getData = getData;
      this.getDefaultNote = getDefaultNote;
    });
})();
