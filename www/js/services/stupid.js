(function (){
  'use strict';

  angular.module('bootcampApp')
    .service('Stupid', function($rootScope, $q, $http){
      function randomIntFromInterval(min,max)
      {
        return Math.floor(Math.random()*(max-min+1)+min);
      }

      function getRandomNumbers(){
        return $http.get("http://www.random.org/integers/", {
          params: {
            num: 9,
            min: 1,
            max: 5000,
            col: 1,
            base: 10,
            format: 'plain',
            rnd: 'new'
          }
        }).then(function(response){
          return _(response.data.split ("\n")).compact().map(function(element){
            return parseInt(element, 10);
          });
        });
      };

      function startNumberGeneration(numbers_to_find){
        numbers_to_find = numbers_to_find.value();
        var deferred = $q.defer();
        var p = deferred.promise;

        $rootScope.$evalAsync(function(){
          var min = 1,
              max = 5000;
          var newNumber = null;
          var found_numbers = [];
          var number_index = 0;

          do {
            newNumber = randomIntFromInterval(min, max);
            number_index = numbers_to_find.indexOf(newNumber);
            if(number_index > -1){
              found_numbers.push(newNumber);
              numbers_to_find.splice(number_index, 1);
            }
          }while(numbers_to_find.length > 0);

          deferred.resolve("Magic numbers are: " + found_numbers);
        });

        return p;
      }

      this.getRandomNumbers = getRandomNumbers;
      this.startNumberGeneration = startNumberGeneration;
    });
})();
