(function () {
  angular.module('bootcampApp')
    .directive('note', function(){
      return {
        templateUrl: 'partials/note.html',
        restrict: 'E'
      };
    });
})();
