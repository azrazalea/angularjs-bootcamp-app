(function () {
  angular.module('bootcampApp')
    .directive('noteForm', function(){
      return {
        templateUrl: 'partials/note_form.html',
        restrict: 'E'
      };
    });
})();
