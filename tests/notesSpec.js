describe('Notes service', function(){
  beforeEach(module('bootcampApp'));
  beforeEach(inject(function(_Notes_){
    Notes = _Notes_;
  }));

  it("cloneNote creates an equal but new copy", function(){
    var note = {title: "Blah", body: "Blah", createdDate: new Date()};
    var new_note = Notes.cloneNote(note);
    expect(note).not.toBe(new_note);
    expect(note).toEqual(new_note);
  });
});
