(function(){
  'use strict';

  var element, parentScope;
  describe('note', function(){
    beforeEach(function(){
      module("bootcampApp");
      module("bootcampAppPartials");
      element = angular.element('<note></note>');


      inject(function($rootScope){parentScope = $rootScope.$new();});
      inject(function($compile) {$compile(element)(parentScope);});
      parentScope.$digest();
    });

    it('has string Created:', function(){
      expect(angular.element(element).html()).toContain('createdDate');
    });
  });
})();
