describe('Notes controller', function(){
  var scope;
  beforeEach(module('bootcampApp'));
  beforeEach(inject(function($rootScope, $controller, _Notes_){
    scope = $rootScope.$new();
    Notes = _Notes_;
    Notes.saveData = function(data){};
    Notes.getData = function(){return [{"title": "Test", "body": "Test"}];};
    $controller('noteCtrl', {Notes:Notes,$scope:scope, defaultNote:{data: {"title": "Placeholder", "body": "Placeholder"}}});
  }));

  it('initializes default notes list', function(){
    expect(scope.notes).not.toBeUndefined();
    expect(scope.notes).toEqual([{"title": "Test", "body": "Test"}]);
  });

  it('has a default new note', function(){
    expect(scope.newNote).not.toBeUndefined();
  });

  it('createNote adds a new note', function(){
    var note = {"title": "new note", "body": "new note"};
    scope.createNote(note);
    expect(scope.notes[scope.notes.length - 1]).toEqual(note);
  });

  it('deleteNote deletes an existing note', function(){
    var note = Notes.cloneNote(scope.notes[0]);
    scope.deleteNote(note);
    expect(scope.notes[0]).not.toEqual(note);
  });

  it('deleteAllNotes removes all notes', function(){
    var note = {"title": "new note", "body": "new note"};
    scope.createNote(note);
    scope.deleteAllNotes();
    expect(scope.notes).toEqual([]);
  });
});
